Feature: LOGIN kasirAja

    Background:
        Given I am on the login page

    Scenario: login with invalid credential
        When I input email "<email>" and password "<password>"
        And I click on button login
        Then I must see error message saying "<errorMessage>"

        Examples:
            | email             | password    | errorMessage                            |
            | tokovelo@vmail.com| 123pwa      | Kredensial yang Anda berikan salah      |
            | tokoveli@vmail.com| 123pw      | Kredensial yang Anda berikan salah      |
            | tokoveli@vmail.com| 123pwa     | Kredensial yang Anda berikan salah      |
        

    Scenario: Login with valid credential
        When I input email "<email>" and password "<password>"
        And I click on button login
        Then I must navigated to dashboard page

        Examples:
            | email              | password    | message  |
            | tokovelo@vmail.com | 123pw      | kasirAja |