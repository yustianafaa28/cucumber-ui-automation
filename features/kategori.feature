Feature: KATEGORI kasirAja

    Scenario: Add new kategori
        Given I already login with email "tokovelo@vmail.com" and password "123pw"
        Then I redirect to the dashboard page
        When I click on menu kategori
        When I click on button tambah
        When I input nama kategori "<namaKategori>" and deskripsi "<deskripsi>"
        When I click on button simpan
        Then I must see a successful message "<sukses>" "<ditambahkan>"

        Examples:
            | namaKategori | deskripsi    | sukses  | ditambahkan      |
            | Sleeve       | Long Sleeve  | success | item ditambahkan |